package com.trashlabs.lime;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Date;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;

import com.trashlabs.lime.annotations.Column;
import com.trashlabs.lime.annotations.GeneratedValue;
import com.trashlabs.lime.annotations.PrimaryKey;
import com.trashlabs.lime.converters.TConverter;

/**
 * This class represents info about a field. It's used by the field readers to determine 
 * what to do with the field in question. 
 */
class FieldInfo {
	
	/**
	 * Basic fields are java primitive or string.
	 */
	public static final int CLASSIFICATION_BASIC = 0; 
	
	/**
	 * Complex fields are user types such as other related models.
	 */
	public static final int CLASSIFICATION_COMPLEX = 1; 
	
	/**
	 * Collection fields derive from java List. 
	 */
	public static final int CLASSIFICATION_COLLECTION = 2; 
	
	private Field field; 
	private boolean isBasic; 
	private boolean isGenerated;
	private boolean isRowId;
	private boolean isPrimaryKey;
	private String name; 
	private int classification; 
	private TConverter converter; 
	
	/**
	 * Get the Field this info model is based on. 
	 * 
	 * @see java.lang.reflect.Field
	 * @return field
	 */
	public Field getField() {
		return field; 
	}
	
	public TConverter getConverter() {
		return converter; 
	}
	
	/**
	 * Get the data type for this field.
	 * 
	 * @return type
	 */
	public Class<?> getType() {
		return field.getType();
	}

	/**
	 * Get the basic status for this field. "Basic" is defined as a primitive Java type or a String. 
	 * 
	 * @return
	 */
	public boolean isBasic() {
		return isBasic;
	}

	/**
	 * Determine if the field is database generated. In other words, does it have the GeneratedValue annotation. 
	 * 
	 * @see com.trashlabs.lime.attributes.GeneratedValue
	 * 
	 * @return
	 */
	public boolean isGenerated() {
		return isGenerated;
	}
	
	/**
	 * Determine if this field is the _rowid_ field from the DataEntity base class.
	 * 
	 * @return
	 */
	public boolean isRowId() {
		return isRowId;
	}

	/**
	 * Get the resolved name for this field. This takes into account the Column annotation. 
	 * 
	 * @see com.trashlabs.lime.attributes.Column
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Return true if the field is a primary key. This considers the PrimaryKey annotation. 
	 * 
	 * @see com.trashlabs.lime.PrimaryKey
	 * 
	 * @return
	 */
	public boolean isPrimaryKey() {
		return isPrimaryKey;
	}
	
	/**
	 * Get the data classification for this field. Classifications are basic, complex, and collection. See
	 * the constants on FieldInfo for a description of each type.
	 * 
	 * @return the classification for this field
	 */
	public int getClassification() {
		return classification; 
	}
	
	/**
	 * Determine if an annotation is present on this field. 
	 * 
	 * @param annotationType
	 * @return
	 */
	public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
		return field.isAnnotationPresent(annotationType);
	}
	
	/**
	 * Get an annotation from this field. 
	 * 
	 * @param annotationType
	 * @return
	 */
	public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
		return field.getAnnotation(annotationType);
	}
	
	/**
	 * Get the string value of this field. Suitable for saving to sqlite. 
	 * 
	 * @param item
	 * @return
	 */
	public Object getValue(Object item) {
		try {
			return field.get(item);
		} catch (Exception e) {
			throw new RuntimeException("Error accessing value of field " + field.getName(), e);
		}
	}
	
	/**
	 * Construct a new FieldInfo object. 
	 * 
	 * @param field
	 */
	FieldInfo(Field field) {
		this.field = field; 
		this.isBasic = field.getType().isPrimitive() || field.getType().isAssignableFrom(Date.class) || field.getType().isAssignableFrom(String.class);
		this.isRowId = field.getName().equals("_rowid_");
		this.isGenerated = field.isAnnotationPresent(GeneratedValue.class) || isRowId;
		this.name = (field.isAnnotationPresent(Column.class)) ? field.getAnnotation(Column.class).value() : field.getName();
		this.isPrimaryKey = field.isAnnotationPresent(PrimaryKey.class);
		this.converter = TConverter.get(field);
		
		if(isBasic) {
			classification = CLASSIFICATION_BASIC;
		} else if (field.getType().isAssignableFrom(List.class)) {
			classification = CLASSIFICATION_COLLECTION;
		} else {
			classification = CLASSIFICATION_COMPLEX;
		}
	}
	
	public void frObj(ContentValues values, Object item) {
		if(converter == null) return; 
		try {
			converter.frObj(values, getName(), item);
		} catch (Exception e) {
			throw new RuntimeException("could not read from object", e);
		}
	}
	
	public void toObj(Cursor cursor, int columnIndex, Object item) {
		if(converter == null) return; 
		try {
			converter.toObj(cursor, columnIndex, item);
 		} catch (Exception e) {
			throw new RuntimeException("could not write to object " + getName(), e);
		}
	}
}
