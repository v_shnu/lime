package com.trashlabs.lime;

import java.util.List;

import android.net.Uri;

/**
 * The DataSet class represents a collection of objects in a data store. Through DataSet, you can 
 * add, remove, and query objects from the store. DataSet objects are typically not created outside 
 * of the DataStore implementation. 
 * 
 * @param <T>
 */
public class DataSet<T extends DataEntity> {
	
	private SqlDataStore store;
	private ClassInfo classInfo;

	/**
	 * Construct a new DataSet. This constructor is typically only used in a field of a DataStore. 
	 * For example: 
	 * 
	 * <pre>
	 * public class ExampleDataStore extends SqlDataStore {
	 *
	 *    public DataSet&lt;Car&gt; cars = new DataSet&lt;Car&gt;(this, Car.class);
	 *    public DataSet&lt;Person&gt; people = new DataSet&lt;Person&gt;(this, Person.class);
	 *    public DataSet&lt;Address&gt; addresses = new DataSet&lt;Address&gt;(this, Address.class);
     *
	 *    public ExampleDataStore(Context context) {
	 *       super(context, "example.db", 14);
	 *    }
	 * }
	 * </pre>
	 * 
	 * @param store
	 * @param type
	 */
	public DataSet(SqlDataStore store, Class<T> type) {
		this.store = store;
		this.classInfo = Schema.getInfo(type);
	}
	
	/**
	 * Fetch the underlying ClassInfo for this data set. 
	 * 
	 * @return
	 */
	public ClassInfo getClassInfo() {
		return classInfo;
	}
	
	/**
	 * Get the content Uri for this content on the data store. 
	 * 
	 * @return
	 */
	public Uri getContentUri() {
		return store.getContentUri(classInfo.getType());
	}
	
	public T find(long id) {
		return new Query<T>(store, classInfo).where("_rowid_=?", String.valueOf(id)).getSingle();
	}
	
	/**
	 * Execute a query on the data store. 
	 * 
	 * @return
	 */
	public Query<T> query() {
		return new Query<T>(store, classInfo);
	}
	
	/**
	 * Add an item to the data store.
	 * 
	 * @param obj
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public long add(T item) {
		return store.add(classInfo, item);
	}
	
	/**
	 * Add a list of items to the data store. 
	 * 
	 * @param items
	 */
	public void add(List<T> items) {
		store.add(classInfo, items);
	}
	
	/**
	 * Remove an object from the data store.
	 * 
	 * @param obj
	 * @return
	 */
	public long remove(T item) {
		return store.remove(classInfo, item);
	}
	
	/**
	 * Remove a list of items from the data store. 
	 * 
	 * @param items
	 */
	public void remove(List<T> items) {
		store.remove(classInfo, items);
	}
	
	/**
	 * remove all items of this type from the data store. 
	 */
	public void removeAll() {
		store.removeAll(classInfo);
	}
	
	/**
	 * Notify observers of a change to this DataSet. 
	 * 
	 * @param context
	 */
	public void notifyChange() {
		store.notifyChange(getContentUri());
	}
	
}
