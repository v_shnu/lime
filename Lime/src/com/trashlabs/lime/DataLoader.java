package com.trashlabs.lime;

import java.lang.ref.WeakReference;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;

/**
 * A basic loader to return a T. This loader has an optional content observer coupling
 * 
 * @param <T>
 */
public abstract class DataLoader<T> extends AsyncTaskLoader<T> {

	private T result;
	final ContentObserver mObserver; 
	private Uri[] contentUris; 
	private MyHandler<T> handler = new MyHandler<T>(this);
	
	/**
	 * Create a new data loader. 
	 * 
	 * @param context
	 * @param contentUri A contentUri to allow the loader to listen for content changes and 
	 * reload appropriately.
	 */
	public DataLoader(Context context, Uri... contentUri) {
		super(context);
		this.contentUris = contentUri; 
		mObserver = new ContentObserver(handler) {

			@Override
			public void onChange(boolean selfChange) {
				handler.sendEmptyMessage(0);
			}
			
		};
		if(contentUris != null) {
			for(Uri uri : contentUris) {
				getContext().getContentResolver().registerContentObserver(uri, true, mObserver);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.content.Loader#onStartLoading()
	 */
	protected void onStartLoading() {
		if(result != null) {
			deliverResult(result);
		} else {
			forceLoad();
		}
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.content.Loader#onReset()
	 */
	@Override
	protected void onReset() {
		super.onReset();
		getContext().getContentResolver().unregisterContentObserver(mObserver);
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.content.Loader#onContentChanged()
	 */
	@Override
	public void onContentChanged() {
		result = null; 
		forceLoad();
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.content.AsyncTaskLoader#loadInBackground()
	 */
	@Override
	public T loadInBackground() {
		result = getResult(); 
		return result; 
	}
	
	/**
	 * Fetch results for this loader. This is the core section managed by the loader. Do 
	 * your work of fetching results from SQLite or a backend here. 
	 * 
	 * @return
	 */
	public abstract T getResult(); 
	
	/**
	 * Handler to receive content changes and reset the loader. It uses a weak reference to the 
	 * loader to avoid memory leaks. 
	 */
	static class MyHandler<T> extends Handler {
		
		WeakReference<DataLoader<T>> loaderRef;
		
		public MyHandler(DataLoader<T> loader) {
			this.loaderRef = new WeakReference<DataLoader<T>>(loader);
		}
		
		@Override
		public void handleMessage(Message msg) {
			DataLoader<T> loader = loaderRef.get();
			if(loader != null) {
				loader.onContentChanged();
			}
		}
	}
}