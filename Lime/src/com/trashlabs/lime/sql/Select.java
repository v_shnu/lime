package com.trashlabs.lime.sql;

import java.util.ArrayList;

import android.text.TextUtils;

/**
 * Select class is a builder for a SQL SELECT statement. 
 */
public class Select {
	
	private ArrayList<String> columns = new ArrayList<String>();
	private ArrayList<Join> joins = new ArrayList<Join>(); 
	private boolean distinct = false; 
	private String from = null; 
	private String where = null; 
	private String[] whereArgs; 
	private String[] groupBy;
	private String[] orderBy; 
	
	public Select() { }
	
	public Select(String... columns) {
		for(String column : columns) this.columns.add(column);
	}
	
	public String[] getArgs() {
		return whereArgs;
	}
	
	public Select addColumns(String... columns) {
		for(String column : columns) this.columns.add(column);
		return this; 
	}
	
	public Select distinct() {
		this.distinct = true;
		return this; 
	}
	
	public Select from(String from) {
		this.from = from; 
		return this; 
	}
	
	public Select leftJoin(String table, String expression) {
		joins.add(new Join("LEFT JOIN", table, expression));
		return this; 
	}
	
	public Select where(String expression) {
		this.where = expression;
		return this; 
	}
	
	public Select where(String expression, String... whereArgs) {
		this.where = expression;
		this.whereArgs = whereArgs; 
		return this; 
	}
	
	public Select groupBy(String... groupBy) {
		this.groupBy = groupBy; 
		return this; 
	}
	
	public Select orderBy(String... orderBy) {
		this.orderBy = orderBy; 
		return this; 
	}
	
	@Override
	public String toString() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ");
		if (distinct) sql.append("DISTINCT ");
		sql.append(TextUtils.join(", ", columns) + " ");
		sql.append("FROM ");
		sql.append(from + " ");
		for(Join join : joins) {
			sql.append(join.toString() + " ");
		}
		if(where != null) {
			sql.append("WHERE ");
			sql.append(where + " ");
		}
		if(groupBy != null) {
			sql.append("GROUP BY ");
			sql.append(TextUtils.join(", ", groupBy) + " ");
		}
		if(orderBy != null) {
			sql.append("ORDER BY ");
			sql.append(TextUtils.join(",  ", orderBy) + " ");
		}
		return sql.toString();
	}
	
	class Join {
		public String operation; 
		public String table; 
		public String expression; 
		
		public Join(String operation, String table, String expression) {
			this.operation = operation; 
			this.table = table; 
			this.expression = expression; 
		}
		
		@Override
		public String toString() {
			return operation + " " + table + " ON " + expression; 
		}
	}

}
