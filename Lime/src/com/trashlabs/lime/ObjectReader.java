package com.trashlabs.lime;

import java.util.ArrayList;


import android.database.Cursor;

/**
 * The ObjectLoader class is responsible for creating objects from cursor rows. 
 * 
 * @param <T> The data type to create from the cursor.
 */
class ObjectReader<T extends DataEntity> {
	
	/**
	 * The type that this reader is capable of reading. 
	 */
	private Class<T> type;
	
	/**
	 * List of field readers to populate each individual field from the cursor. 
	 */
	private ArrayList<FieldReader> fieldReaders = new ArrayList<FieldReader>();
	
	/**
	 * Flag to determine if the cursor contains any fields for this object. If not, this 
	 * reader will return null upon calling read().
	 */
	private boolean isValid = false; 
	
	/**
	 * Create a new object loader based on a cursor and a type. 
	 * 
	 * @param cursor
	 * @param type
	 */
	public ObjectReader(Cursor cursor, Class<T> type) {
		this("", cursor, type);
	}
	
	/**
	 * Create a new object reader based on a cursor and a type using a specific column prefix.
	 * 
	 * @param prefix
	 * @param cursor
	 * @param type
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ObjectReader(String prefix, Cursor cursor, Class<T> type) {
		// prepare the column indexes and field readers for this type
		this.type = type;
		ClassInfo info = Schema.getInfo(type);
		for(FieldInfo fieldInfo : info.getFields()) {
			switch(fieldInfo.getClassification()) {
			case FieldInfo.CLASSIFICATION_BASIC :
				String columnName = prefix + fieldInfo.getName();
				int columnIndex = cursor.getColumnIndex(columnName);
				if(columnIndex == -1) continue;
				isValid = true; //the cursor did contain at least one field for this object
				fieldReaders.add(new BasicFieldReader(fieldInfo, columnIndex));
				break; 
			case FieldInfo.CLASSIFICATION_COMPLEX:
				if(prefix.contains(fieldInfo.getName())) break; 
				String newpfx = prefix + fieldInfo.getName();
				ObjectReader<?> childReader = new ObjectReader(newpfx, cursor, fieldInfo.getType());
				if(childReader.isValid)
					fieldReaders.add(new ComplexFieldReader(fieldInfo, childReader));
				break;
			case FieldInfo.CLASSIFICATION_COLLECTION:
				//TODO: make this thing read lazy load child collections
				break;
			}
			
		}
	}
	
	/**
	 * Create a new object from the existing cursor. Field readers may cascade and result in this
	 * object having populated object child fields.  
	 * 
	 * @param cursor
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public T read(Cursor cursor) throws InstantiationException, IllegalAccessException {
		if(!isValid) return null; // this cursor contained no fields for this object
		T item = type.newInstance();
		for(FieldReader reader : fieldReaders)
			reader.read(cursor, item);
		return item;
	}
	
	/**
	 * Interface to read a field from a cursor.
	 */
	interface FieldReader {
		public void read(Cursor cursor, Object item) throws InstantiationException, IllegalAccessException;
	}
	
	/**
	 * Reads a basic field value from a cursor.
	 */
	class BasicFieldReader implements FieldReader {
		
		private FieldInfo info; 
		private int columnIndex;
		
		public BasicFieldReader(FieldInfo info, int columnIndex) {
			this.info = info;
			this.columnIndex = columnIndex;
		}
		
		@Override
		public void read(Cursor cursor, Object item) {
			info.toObj(cursor, columnIndex, item);
		}
	}
	
	/**
	 * Reads a complex object from a child. 
	 */
	class ComplexFieldReader implements FieldReader {
		
		private ObjectReader<?> reader; 
		private FieldInfo info; 
		
		public ComplexFieldReader(FieldInfo info, ObjectReader<?> reader) {
			this.reader = reader; 
			this.info = info;
		}

		@Override
		public void read(Cursor cursor, Object item) throws InstantiationException, IllegalAccessException {
			Object child = reader.read(cursor);
			info.getField().set(item, child);
		}
	}
}
