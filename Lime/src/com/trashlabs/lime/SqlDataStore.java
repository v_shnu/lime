package com.trashlabs.lime;

import java.lang.reflect.Field;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**
 * The SqlDataStore represents a sql database in your application. Override this class and add 
 * dbset fields to provide data store functionality to your app. Example:
 * 
 * <pre>
 * public class AppDb extends SqlDataStore {
 *	
 *    public DataSet<Car> cars = new DataSet<Car>(this, Car.class);
 *    public DataSet<Product> products = new DataSet<Product>(this, Product.class);
 *    public DataSet<Person> people = new DataSet<Person>(this, Person.class);
 *
 *    public AppDataStore(Context context) {
 *       super(context, "example.db", 28);
 *    }
 *
 *}
 * </pre>
 */
public abstract class SqlDataStore {
	
	private volatile static SqlHelper helper; 
	private SQLiteDatabase db; 
	private Context context;
	
	/**
	 * Constructor for a new data store. Typically this constructor is overridden by your implementation class
	 * such that dbname and version are hidden from the rest of the app. For example: 
	 * 
	 * <pre>	
	 * public ExampleDataStore(Context context) {
	 *    super(context, "example.db", 14);
	 * }
	 * </pre>
	 * 
	 * @param context The current context
	 * @param dbName Database name on the file system
	 * @param version Database version. Increment this when schema changes
	 */
	public SqlDataStore(Context context, String dbName, int version) {
		checkHelper(context, dbName, version);
		this.context = context.getApplicationContext();
	}
	
	/**
	 * Fetch the instance of the internal SQLite database.
	 * @return
	 */
	public SQLiteDatabase getDb() {
		checkDatabase();
		return db; 
	}
	
	
	public DataSet<?> getDbSet(Class<?> type) throws Exception {
		Field[] fields = this.getClass().getFields();
		for(Field field : fields) {
			boolean isDbSet = field.getType().equals(DataSet.class);
			if(!isDbSet) continue; 
			DataSet<?> dbset = (DataSet<?>)field.get(this);
			if(dbset.getClassInfo().getType() == type) return dbset; 
		}
		return null;
	}
	/**
	 * Fetch the content URI for a type handle by this data store. 
	 * 
	 * @param type
	 * @return A uri like content://com.mypackage/mytype
	 */
	public Uri getContentUri(Class<?> type) {
		return Uri.parse("content://" + context.getPackageName() + "/" + type.getSimpleName());
	}
	
	/**
	 * Synchronized method to set up the SqlHelper. 
	 * 
	 * @param context The current context
	 * @param dbName Database name on the file system
	 * @param version Database version. Increment this when schema changes
	 */
	private synchronized void checkHelper(Context context, String dbName, int version) {
		if(helper == null) {
			if(context == null) throw new RuntimeException("Context is null. SqlDataStore should be constructed with a non-null context. If this is a fragment, try initializing SqlDataStore in OnViewCreated.");
			helper = new SqlHelper(context, this, dbName, null, version);
		}
	}
	
	/**
	 * Private method to check the state of database setup.
	 */
	private synchronized void checkDatabase() {
		if(db == null) db = helper.getWritableDatabase();
	}
	
	/**
	 * Start a transaction on the underlying database. 
	 */
	public void beginTransaction() {
		db.beginTransaction();
	}
	
	/**
	 * End a transaction on the underlying database. 
	 */
	public void endTransaction() {
		db.endTransaction();
	}
	
	/**
	 * Execute a raw SQL query on the underlying database. 
	 * @param sql
	 */
	public void execSQL(String sql) {
		checkDatabase();
		db.execSQL(sql);
	}
	
	/**
	 * Execute a raw SQL query on the underlying database.
	 *  
	 * @param sql
	 * @param selectionArgs
	 * @return
	 */
	public Cursor rawQuery(String sql, String[] selectionArgs) {
		checkDatabase();
		return db.rawQuery(sql, selectionArgs);
	}


	/**
	 * Insert a list of objects into the database. This operation is wrapped in a SQL 
	 * transaction for speed. 
	 * 
	 * @param classInfo
	 * @param items
	 */
	public synchronized <T extends DataEntity> void add(ClassInfo classInfo, List<T> items) {
		checkDatabase();
		db.beginTransaction();
		try {
			for(T item : items) add(classInfo, item);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}
	
	/**
	 * Add a single object to the database. 
	 * 
	 * @param classInfo
	 * @param item
	 * @return
	 */
	public synchronized <T extends DataEntity> long add(ClassInfo classInfo, T item) {
		checkDatabase();
		ContentValues values = new ContentValues(); 
		
		if(item._rowid_ == -1) {
			// insert the item, so avoid generated fields like _rowid_
			for(FieldInfo field : classInfo.getBasicFields()) {
				if(!field.isGenerated())
					field.frObj(values, item);
			}
		} else {
			// update the item, so include generated fields like _rowid_
			for(FieldInfo field : classInfo.getBasicFields()) {
				field.frObj(values, item);
			}
		}
		long result = db.insertWithOnConflict(classInfo.getTableName(), null, values, SQLiteDatabase.CONFLICT_REPLACE);
		item._rowid_ = result;
		//TODO: need to reload the object entirely to fetch the db-generated values from the modified row.
		return result; 
	}
	
	/**
	 * Remove a list of items from the database. This operation is enclosed in a transaction 
	 * for speed. 
	 * 
	 * @param classInfo
	 * @param items
	 */
	public synchronized <T extends DataEntity> void remove(ClassInfo classInfo, List<T> items) {
		checkDatabase();
		db.beginTransaction();
		try {
			for(T item : items) remove(classInfo, item);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}
	
	/**
	 * Remove a single object from the data store. 
	 * 
	 * @param classInfo
	 * @param item
	 * @return The number of rows affected or 0. 
	 */
	public synchronized <T extends DataEntity> int remove(ClassInfo classInfo, T item) {
		checkDatabase();
		String[] whereArgs = new String[] { String.valueOf(item._rowid_) };
		return db.delete(classInfo.getTableName(), "_rowid_=?", whereArgs);
	}
	
	synchronized int remove(String tableName, String whereClause, String... whereArgs) {
		return db.delete(tableName, whereClause, whereArgs);
	}
	
	/**
	 * Remove all objects of this type from the data store. 
	 * 
	 * @param classInfo
	 * @return The number of rows affected or 0. 
	 */
	public synchronized <T extends DataEntity> int removeAll(ClassInfo classInfo) {
		checkDatabase();
		return db.delete(classInfo.getTableName(), "1", null);
	}

	/**
	 * Notify all content observers that there was a change on this Uri. 
	 * @param uri
	 */
	void notifyChange(Uri uri) {
		context.getContentResolver().notifyChange(uri, null);
	}
	
}
