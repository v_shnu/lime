package com.trashlabs.lime.converters;

import java.lang.reflect.Field;

import android.content.ContentValues;
import android.database.Cursor;

public class ShortConverter extends TConverter {

	public ShortConverter(Field field) {
		super(field);
	}

	@Override
	public void toObj(Cursor cursor, int columnIndex, Object item)
			throws IllegalArgumentException, IllegalAccessException,
			InstantiationException {
		field.set(item, cursor.getShort(columnIndex));
		
	}

	@Override
	public void frObj(ContentValues values, String columnName, Object item)
			throws IllegalArgumentException, IllegalAccessException,
			InstantiationException {
		values.put(columnName, field.getShort(item));
	}

}
